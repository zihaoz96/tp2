package fr.univavignon.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.ContextMenu;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    WineDbHelper wineDbHelper = new WineDbHelper(this);


    Cursor cursor = null;

    SimpleCursorAdapter adapter;

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Initialiser les donnees
//        wineDbHelper.populate();

        // Clique sauter l'activite
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent wineIntent = new Intent(MainActivity.this, WineActivity.class);
                startActivity(wineIntent);
            }
        });

        cursor = wineDbHelper.fetchAllWines();
        listView = (ListView) findViewById(R.id.listView);

        // Long click Listener
        listView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;

                int position = info.position;
                menu.add(0,position,0,"Supprime");

            }
        });

        // Click Listener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, WineActivity.class);
                final Cursor c = (Cursor) parent.getItemAtPosition(position);

                Wine wineSend = WineDbHelper.cursorToWine(c);

                intent.putExtra("wineSend", wineSend);
                startActivity(intent);
            }
        });



        /* methode de Arraylist

        ArrayList<String> arrayList = new ArrayList<String>();
        ArrayList<String> arrayList2 = new ArrayList<String>();
        if (cursor.moveToFirst()){
            arrayList.add(cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_NAME)));
            arrayList2.add(cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_LOC)));
            while (cursor.moveToNext()){
                arrayList.add(cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_NAME)));
                arrayList2.add(cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_LOC)));
            }
        final ArrayAdapter<String> adapter = new SimpleCursorAdapter<String>(this, android.R.layout.simple_list_item_1, arrayList);
        */

        adapter = new SimpleCursorAdapter(
                                                this,
                                                android.R.layout.simple_list_item_2,
                                                cursor,
                                                new String[] {WineDbHelper.COLUMN_NAME,WineDbHelper.COLUMN_WINE_REGION},
                                                new int[] {android.R.id.text1,android.R.id.text2}
                                                );
        listView.setAdapter(adapter);

        Intent myIntent = getIntent();
        boolean alert = myIntent.getBooleanExtra("alert",false);
        if (alert){
            String title = "Ajout impossible";
            String message = "Un vin portant le meme nom existe deja dans la base de donnees.";

            WineActivity.afficherDialog(title, message, this);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        cursor.moveToPosition(item.getItemId());
        wineDbHelper.deleteWine(cursor);

        adapter.changeCursor(wineDbHelper.fetchAllWines());
        cursor = wineDbHelper.fetchAllWines();
        //adapter.notifyDataSetChanged();


        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
