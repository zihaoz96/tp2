package fr.univavignon.tp2;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class WineActivity extends AppCompatActivity {

    Button save;

    EditText name,region,loc,climate,plante;

    Wine newWine = null;
    WineDbHelper wineDbHelper = new WineDbHelper(this);

    String nameStr,regionStr,locStr,climatStr,planteStr;

    Wine wineGet = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        Intent intent = getIntent();

        wineGet = (Wine) intent.getParcelableExtra("wineSend");

        save = (Button) findViewById(R.id.button);
        name = (EditText) findViewById(R.id.wineName);
        region = (EditText) findViewById(R.id.editWineRegion);
        loc = (EditText) findViewById(R.id.editLoc);
        climate = (EditText) findViewById(R.id.editClimate);
        plante = (EditText) findViewById(R.id.editPlantedArea);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nameStr = name.getText().toString();
                regionStr = region.getText().toString();
                locStr = loc.getText().toString();
                climatStr = climate.getText().toString();
                planteStr = plante.getText().toString();

                Intent intent = new Intent(WineActivity.this, MainActivity.class);

                if (nameStr.isEmpty()){
                    //|| regionStr.isEmpty() || locStr.isEmpty() || climatStr.isEmpty() || planteStr.isEmpty()
                    String title = "Sauvgarde impossible:";
                    String message = "Le nom du vin doit etre non vide !";

                    afficherDialog(title,message,WineActivity.this);
                }else {
                    if (wineGet != null) {
                        wineGet.setTitle(nameStr);
                        wineGet.setClimate(climatStr);
                        wineGet.setLocalization(locStr);
                        wineGet.setRegion(regionStr);
                        wineGet.setPlantedArea(planteStr);
                        int rows = wineDbHelper.updateWine(wineGet);
                        Toast.makeText(WineActivity.this, "Votre modification bien enregistre !", Toast.LENGTH_SHORT).show();
                    }else {
                        newWine = new Wine(nameStr, regionStr, locStr, climatStr, planteStr);
                        if (wineDbHelper.addWine(newWine) == false) {
                            intent.putExtra("alert", true);
                        } else {
                            Toast.makeText(WineActivity.this, "Bien enregistrer !", Toast.LENGTH_SHORT).show();
                        }
                    }
                    finish();
                    startActivity(intent);
                }
            }
        });
    }

    public static void afficherDialog(String title, String message, Context context){
        final AlertDialog alert = new AlertDialog.Builder(context).create();
        alert.setTitle(title);
        alert.setMessage(message);
        alert.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                alert.dismiss();
            }
        }, 3000);
    }
}
