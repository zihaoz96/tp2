package fr.univavignon.tp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class WineDbHelper extends SQLiteOpenHelper {

    private static final String TAG = WineDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "wine.db";

    public static final String TABLE_NAME = "cellar";

    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WINE_REGION = "region";
    public static final String COLUMN_LOC = "localization";
    public static final String COLUMN_CLIMATE = "climate";
    public static final String COLUMN_PLANTED_AREA = "publisher";


    public WineDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //SQLiteDatabase db = this.getWritableDatabase();
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        String create = String.format("CREATE TABLE '%s' " +
                "(" +
                    "'%s' INTEGER PRIMARY KEY," +
                    "'%s' TEXT NOT NULL," +
                    "'%s' TEXT NOT NULL," +
                    "'%s' TEXT NOT NULL," +
                    "'%s' TEXT NOT NULL," +
                    "'%s' TEXT NOT NULL," +
                    "UNIQUE (" + COLUMN_NAME + ", " + COLUMN_WINE_REGION + ") ON CONFLICT ROLLBACK" +
                ");", TABLE_NAME, _ID, COLUMN_NAME, COLUMN_WINE_REGION, COLUMN_LOC, COLUMN_CLIMATE, COLUMN_PLANTED_AREA);
        db.execSQL(create);



    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            db.execSQL("drop table if exists " + TABLE_NAME);
            onCreate(db);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


   /**
     * Adds a new wine
     * @return  true if the wine was added to the table ; false otherwise (case when the pair (name, region) is
     * already in the data base
     */
    public boolean addWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = String.format("SELECT * FROM %s WHERE %s = '%s' AND %s = '%s'",TABLE_NAME,COLUMN_NAME,wine.getTitle(),COLUMN_WINE_REGION,wine.getRegion());
        Cursor cursor = db.rawQuery(sql, null);

        if (cursor.getCount() > 0){
            return false;
        }
        ContentValues values = new ContentValues();
        //values.put(_ID, wine.getId());
        values.put(COLUMN_WINE_REGION, wine.getRegion());
        values.put(COLUMN_NAME, wine.getTitle());
        values.put(COLUMN_LOC, wine.getLocalization());
        values.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());
        values.put(COLUMN_CLIMATE, wine.getClimate());
        long resId = db.insert(TABLE_NAME, null, values);

        db.close();
        if (resId == -1){return false;}
        return true;
    }

    /**
     * Updates the information of a wine inside the data base
     * @return the number of updated rows
     */
    public int updateWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_WINE_REGION, wine.getRegion());
        values.put(COLUMN_NAME, wine.getTitle());
        values.put(COLUMN_LOC, wine.getLocalization());
        values.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());
        values.put(COLUMN_CLIMATE, wine.getClimate());
        String[] args = {Long.toString(wine.getId())};

        return db.update(TABLE_NAME, values, _ID+"=?",args);
    }

    /**
     * Returns a cursor on all the wines of the library
     */
    public Cursor fetchAllWines() {
        SQLiteDatabase db = this.getReadableDatabase();

	    Cursor cursor = null;

        String sql = String.format("SELECT * FROM %s",TABLE_NAME);
        cursor = db.rawQuery(sql,null);
        
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

     public void deleteWine(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME,_ID + "=" + cursor.getLong(cursor.getColumnIndex(_ID)),null);
        db.close();
    }

     public void populate() {
        Log.d(TAG, "call populate()");
        addWine(new Wine("Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"));
        addWine(new Wine("Arbois", "Jura", "Jura", "continental et montagnard", "812"));
        addWine(new Wine("Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"));
        addWine(new Wine("Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"));
        addWine(new Wine("Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"));
        addWine(new Wine("Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"));
        addWine(new Wine("Bandol", "Provence", "Var", "méditerranéen", "1500"));
        addWine(new Wine("Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"));
        addWine(new Wine("Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"));
        addWine(new Wine("Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"));
        addWine(new Wine("Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"));



        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }


    public static Wine cursorToWine(Cursor cursor) {
        Wine wine = null;
        if (cursor != null){
            wine = new Wine(
                    cursor.getLong(cursor.getColumnIndex(_ID)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_NAME)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_WINE_REGION)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_LOC)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_CLIMATE)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_PLANTED_AREA))
                    );
        }
	    // build a Wine object from cursor
        return wine;
    }


}
